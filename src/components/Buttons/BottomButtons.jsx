import React from 'react';
import { _ } from '../../lib';
import CMButton from './CMButton';

export default class BottomButtons extends React.PureComponent {
	render() {
		const { leftBtnClick, rightBtnClick, leftBtnText, rightBtnText, leftHide, rightHide } = this.props;
		return (
			<div className="btn_area">
				<div className="f_right">
					{!leftHide ? (
						<CMButton className="btn_rb_l" onClick={leftBtnClick}>
							{leftBtnText}
						</CMButton>
					) : null}
					{!rightHide ? (
						<CMButton className="btn_r_l ml10" onClick={rightBtnClick}>
							{rightBtnText}
						</CMButton>
					) : null}
				</div>
			</div>
		);
	}
}
