import React from 'react';

const CMButton = ({ style, className, id, onClick, params = [], children }) => {
	const preventDefault = (e) => {
		e.preventDefault();
		onClick ? onClick(...params) : alert('Not Exists Function');
	};
	return (
		<a href="" id={id} style={style} className={className} onClick={preventDefault}>
			{children}
		</a>
	);
};

export default CMButton;
