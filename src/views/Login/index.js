import React from 'react';
import { observer, inject } from 'mobx-react';
import _ from 'lodash';

@inject((stores) => ({
	userStore: stores.userStore
}))
@observer
export default class Login extends React.Component {
	state = {
		usernameOrEmail: '',
		password: '',
		isSaved: false
	};

	componentDidMount() {
		if (!_.isEmpty(localStorage.getItem('userId'))) {
			this.setState((state) => ({
				usernameOrEmail: localStorage.getItem('userId'),
				isSaved: true
			}));
		}

		// login motion
		document.body.classList.add('login');
		setTimeout(function() {
			document.body.classList.add('ef-on');
		}, 500);
	}

	componentWillUnmount() {}

	login = async (e) => {
		e.preventDefault();
		const { userStore } = this.props;
		const { usernameOrEmail, password } = this.state;
		await userStore.login(usernameOrEmail, password);
	};

	inputEventHandler = (e) => {
		const { target } = e;
		const eventType = {
			text: { usernameOrEmail: target.value },
			password: {
				password: target.value
			},
			checkbox: { isSaved: target.checked }
		};

		this.setState((state) => ({
			...state,
			...eventType[target.type]
		}));
	};

	enterKeyEvent = (e) => {
		if (_.includes(e.key, 'Enter')) {
			this.login(e);
		}
	};

	render() {
		const { usernameOrEmail, password, isSaved } = this.state;

		return (
			<React.Fragment>
				<div className="login_bg" />
				<div className="login_box">
					asdasda
					<div className="login_box_top">
						<div className="login_ver">v1.0</div>
						<img src="img/component/lbstss_login_logo.png" alt="" />
					</div>
					<div className="login_form">
						<form>
							<label htmlFor="login_id">아이디 입력</label>
							<input
								type="text"
								id="login_id"
								placeholder="아이디입력"
								name="usernameOrEmail"
								value={usernameOrEmail}
								onKeyUp={this.enterKeyEvent}
								onChange={this.inputEventHandler}
							/>
							<label htmlFor="login_pwd">비밀번호 입력</label>
							<input
								type="password"
								id="login_pwd"
								placeholder="비밀번호 입력"
								name="password"
								value={password}
								onKeyUp={this.enterKeyEvent}
								onChange={this.inputEventHandler}
							/>
							<button>
								<img src="img/component/login-download.png" alt="" />다운로드
							</button>
							<button id="login_btn" onClick={this.login}>
								<img src="img/component/login-enter.png" alt="" />로그인
							</button>
							<input
								id="login_auto"
								type="checkbox"
								name="isSaved"
								checked={isSaved}
								onChange={this.inputEventHandler}
							/>
							<label htmlFor="login_auto">자동 로그인</label>
						</form>
					</div>
				</div>
			</React.Fragment>
		);
	}
}
