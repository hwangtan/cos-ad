import { types, flow, getEnv } from 'mobx-state-tree';
import { axios } from '../lib';
import apiPath from './api';

const User = types
	.model('User', {
		isAuthenticated: types.optional(types.maybe(types.boolean), false),
		appLoaded: types.optional(types.boolean, false)
	})
	.views((self) => ({
		get appStatus() {
			return self.appLoaded;
		}
	}))
	.actions((self) => {
		const login = flow(function*(usernameOrEmail, password) {
			try {
				const response = yield axios.post(apiPath.login, {
					usernameOrEmail,
					password
				});
				const { accessToken, tokenType } = response.data;
				localStorage.setItem('userId', usernameOrEmail);
				localStorage.setItem('accessToken', accessToken);
				localStorage.setItem('tokenType', tokenType);
				window.location.reload();
			} catch (e) {
				throw new Error('JS Error');
			}
		});

		const isAlreadyLoggedIn = flow(function*() {
			try {
				yield getEnv(self).axios.get(apiPath.loggedIn);
				self.isAuthenticated = true;
			} catch (e) {
				self.isAuthenticated = false;
			} finally {
				self.appLoaded = true;
			}
		});

		const logout = () => {
			localStorage.clear('');
			window.location.reload();
		};

		return { login, isAlreadyLoggedIn, logout };
	});

export default User;
