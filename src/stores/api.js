export default {
	login: 'auth/signin',
	loggedIn: 'user/me'
};
