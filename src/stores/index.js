import makeInspectable from 'mobx-devtools-mst';
import { types } from 'mobx-state-tree';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
import { authenticatedAxios } from '../lib';

import UserStore from './user-store';
import createBrowserHistory from 'history/createBrowserHistory';

const Store = types.model('Store', {
	userStore: types.optional(UserStore, makeInspectable(UserStore.create())),
	router: types.optional(types.frozen)
});

const routingStore = new RouterStore();

export const history = syncHistoryWithStore(createBrowserHistory(), routingStore);

export default Store.create(
	{
		router: routingStore
	},
	{ axios: authenticatedAxios }
);
