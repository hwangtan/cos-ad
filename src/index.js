import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import { Router } from 'react-router';
import Stores, { history } from './stores';
import Routes from './routes';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
	<Provider {...Stores}>
		<Router history={history}>
			<Routes />
		</Router>
	</Provider>,
	document.getElementById('root')
);

registerServiceWorker();
