import moment from 'moment';
import va from 'validator';
import ax from 'axios';

const DATE_FORMAT_FULL = 'YYYY-MM-DD HH:mm:ss';

const DATE_FORMAT_FRAGMENT = 'YYYY-MM-DD';

const DATE_FORMAT_FOR_SEARCH = 'YYYY/MM/DD';

const BASEURL = '';

export const toDateStrForSearch = (oneMonthAgo = false) => {
	let currentDay = moment();
	if (oneMonthAgo) {
		currentDay = moment().subtract(1, 'month');
	}
	return currentDay.format(DATE_FORMAT_FOR_SEARCH);
};

export const toDateStr = (valueOf, isFull = true) =>
	moment(valueOf).format(isFull ? DATE_FORMAT_FULL : DATE_FORMAT_FRAGMENT);

export const axios = ((baseURL) => {
	return ax.create({ withCredentials: false, baseURL });
})(BASEURL);

export const authenticatedAxios = ((baseURL) => {
	return ax.create({
		headers: {
			authorization: `${localStorage.getItem('tokenType')} ${localStorage.getItem('accessToken')}`
		},
		mode: 'no-cors',
		withCredentials: false,
		baseURL
	});
})(BASEURL);

export const validator = {
	isEmail: (str) => va.isEmail(str)
};
