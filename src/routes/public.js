import React from 'react';
import { Route, Redirect } from 'react-router';

const PublicRoute = ({ component: Component, isAuthenticated, ...rest }) =>
	isAuthenticated ? <Redirect to="/" /> : <Route {...rest} render={(props) => <Component {...props} />} />;

export default PublicRoute;
