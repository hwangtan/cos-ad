import React from 'react';
import { Switch, withRouter } from 'react-router';
import routes from './routes';
import PrivateRoute from './private';
import PublicRoute from './public';
import { observer, inject } from 'mobx-react';
import Lottie from 'react-lottie';
import * as animationData from './loader.json';

@inject((stores) => ({ userStore: stores.userStore }))
@observer
class Routes extends React.Component {
	render() {
		this.props.userStore.isAlreadyLoggedIn();
		const defaultOptions = {
			loop: true,
			autoplay: true,
			animationData: animationData,
			rendererSettings: {
				preserveAspectRatio: 'xMidYMid slice'
			}
		};

		return this.props.userStore.appLoaded ? (
			<Switch>
				{routes.map((route, i) => {
					return route.auth ? (
						<PrivateRoute key={i} {...route} isAuthenticated={this.props.userStore.isAuthenticated} />
					) : (
						<PublicRoute key={i} {...route} isAuthenticated={this.props.userStore.isAuthenticated} />
					);
				})}
			</Switch>
		) : (
			<Lottie
				options={defaultOptions}
				height={200}
				width={200}
				isStopped={false}
				style={{
					position: 'absolute',
					top: '40%',
					left: '47%'
				}}
			/>
		);
	}
}

export default withRouter(Routes);
