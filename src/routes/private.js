import React from 'react';
import { Route, Redirect } from 'react-router';
import Layout from '../components/Layout';

const PrivateRoute = ({ component: Component, isAuthenticated, layout, menuGroup, ...rest }) => {
	return (
		<Route
			{...rest}
			render={(props) =>
				isAuthenticated ? layout ? (
					<Layout match={props.match} menuGroup={menuGroup}>
						<Component p={isAuthenticated} {...props} />
					</Layout>
				) : (
					<Component p={isAuthenticated} {...props} />
				) : (
					<Redirect
						to={{
							pathname: '/login',
							state: { from: props.location }
						}}
					/>
				)}
		/>
	);
};

export default PrivateRoute;
