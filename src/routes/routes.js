import Login from '../views/Login';
import Report from '../views/Report';

const routes = [
	{
		path: '/login',
		exact: true,
		auth: false,
		component: Login,
		layout: false
	},
	{
		path: '/',
		exact: true,
		strict: false,
		auth: true,
		component: Report,
		layout: true
	}
];

export default routes;
